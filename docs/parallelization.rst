.. _parralelization:

===============
Parralelization
===============

These are the options that go in **[Parallelization]** section of the config.ini file.


**options**:

   **master_addr:**
   
     kind: ``string`` 

     default: ``127.0.0.1``

   **master_port:**
 
     kind: ``string``

     default: ``1234``

   **dynamics:** 

    kind: ``boolean``

    default: ``False``

    **mp_start_method:**

     kind: ``string``

     default: ``spawn``

     values:

        ``-fork``

        ``-spawn``

   **process_num:** Takes an integer as the number of parallel jobs to perform.
 
     kind: ``integer``

     default: ``1``

   **batch_num:** Takes an integer as the number of batches per process.

     kind: ``integer``

     default: ``100``

Total number of batches are equal to 'process_number * batch_number'.

   **thread_num:**

     kind:  ``integer``

     default: ``1``

   **device_type**: 

     kind: ``string``

     default: ``CPU``
   



