.. _machinelearningmodel:

====================
MachineLearningModel
====================

These are the options that go in **[MachineLearningModel]** section of the config.ini file.

**model_type**: The type of machine learning protocol to be used.

    default: ``neural_network``

    options:

        ``neural_network``: Neural Network


**hidden_layers**: Takes an integer list for the number of hidden layers and nodes

    default: ``20 10``


