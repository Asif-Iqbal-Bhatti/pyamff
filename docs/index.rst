.. PyAMFF documentation master file, created by
   sphinx-quickstart on Sat Jun  6 15:26:44 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PyAMFF: Python Atom-Centered Machine Learning Force Field
=========================================================

PyAMFF is a set of tools for fitting and using atomistic machine learning potentials.

Webpage: https://pyamff.gitlab.io/pyamff/index.html


.. image:: fig/NN.png
   :align: center


Requirements:
=============
* Python_ 3.7 or later
* Pytorch_ 1.7.0
* ASE_ 
* NumPy_
* PyYAML_
* Pickle_

Optional:
---------
* gcc-gfortran_


Installation:
=============
* Using git with ssh::

    $ git clone git@gitlab.com:pyamff/pyamff.git

* Using git with HTML::

    $ git clone https://gitlab.com/pyamff/pyamff.git

* To build fortran modules::

    $ cd pyamff/pyamff/ 
    $ make

* To build the PyAMFF potential for EON::
    
    $ cd pyamff/pyamff/
    $ make EON=1
Once you have compiled the library, copy libAMFF.a to the eon/client/potentials/PyAMFF directory.

* The eon client can be built in eon/client/ using ::

    $ make PYAMFF_POT=1
    $ cp eonclient ../bin/

*Any PyAMFF generated potentials can then be run in eon by setting the potential flag in the EON config.ini file as ::
    
    potential = pyamff

* Make sure to add /path/to/pyamff/bin to your $PATH and /path/to/pyamff/ to your $PYTHONPATH


.. note::
    After installation, you can go to /pyammf/tests/ and run *run_tests.sh* to make sure the build was successful



The PyAMFF team
===============

Henkelman Group (UT Austin)
Lei Li Group (SUSTC)


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _Python: http://www.python.org/
.. _NumPy: http://docs.scipy.org/doc/numpy/reference/
.. _ASE: https://gitlab.com/ase/ase
.. _Pytorch: https://pytorch.org/
.. _PyYAML: https://pypi.org/project/PyYAML/
.. _Pickle: https://pypi.org/project/pickle-mixin/
.. _gcc-gfortran: https://gcc.gnu.org/wiki/GFortran


