.. _fingerprints:

============
Fingerprints
============

These are the options that go in **[Fingerprints]** section of the config.ini file.

**fp_type**: The type of fingerprint to be calculated.

    kind: ``string``

    default: ``BP``

    values:

        ``-BP``: Behler-Parinello symmetry functions (G1 and G2)

**fp_engine**: 
 
    kind: ``string``

    default: ``Fortran``

    values: 

        ``-Fortran``
        
        ``-OldFortran``


**fp_parameter_file**: Name of fingerprint parameter file.

    kind: ``string``

    default: ``fpPara.dat``


**fp_batch_mode**: 

    kind: ``boolean``

    default: ``True``

**fp_batch_num**:

    kind: ``integer``

    default: ``10``  

**fp_use_existing**:

    kind: ``boolean``

    default: ``False``

**fp_file**:

    kind: ``string``

    default: ``fps.pckl``






