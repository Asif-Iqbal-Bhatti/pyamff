echo "Running LJ9 Training Test"
#../../bin/pyamff 
pyamff 

if [[ $(tail -2 pyamff.log | head -1) = *'1    0.24982686   0.07140235   0.00205484'* ]]; then
    echo "Passed LJ9"
else
    echo 'Failed LJ9 -- incorrect output values'
    exit 1
fi

echo ""
echo ""

