from pyamff.pyamffCalc import pyamffCalc
from ase.io import read, Trajectory 

f = open('FU', 'w')

images = read("train.traj", index=":")

for atoms in images:
    #set calculator
    calc = pyamffCalc('../../../pyamff.pt')
    atoms.set_calculator(calc)
    #write force and energy to file readable by EON
    U = atoms.get_potential_energy()
    F = atoms.get_forces()
    f.write('%f \n' % U)
    for i in range(len(F)):
        f.write("%f %f %f\n" % (F[i][0], F[i][1], F[i][2]))


f.close()
