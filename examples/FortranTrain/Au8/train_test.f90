PROGRAM testAu8
  USE training
  IMPLICIT NONE
  ! This program is only for testing energy training for Pd3H2 (single image) 
  CHARACTER*8 :: opt_type
  INTEGER :: i
  INTEGER, PARAMETER :: max_epoch=5
  INTEGER, PARAMETER :: nAtoms=8, nelement=1
  INTEGER, DIMENSION(nAtoms) :: atomicNumbers
  INTEGER, DIMENSION(nelement) :: uniqueNrs
  DOUBLE PRECISION, DIMENSION(nAtoms,3) :: pos_car
  REAL, DIMENSION(9) :: box
  DOUBLE PRECISION :: force_coeff

  ! Set atomic numbers and unique element numbers
  atomicNumbers=(/79, 79, 79, 79, 79, 79, 79, 79/)
  uniqueNrs=(/79/)

  ! Set positions in cartesian 
  pos_car(1,1:3)=(/7.04265,  6.3964,   7.4517/)
  pos_car(2,1:3)=(/9.58385,  7.1238,   6.7005/) 
  pos_car(3,1:3)=(/8.48345,  7.4005,   4.2486/)
  pos_car(4,1:3)=(/7.18605,  8.8166,   6.1653/)
  pos_car(5,1:3)=(/6.64055,  7.2958,  10.7514/)
  pos_car(6,1:3)=(/9.26035,  6.0553,   9.2278/)
  pos_car(7,1:3)=(/8.37835,  8.9447,   8.7524/)
  pos_car(8,1:3)=(/5.41615,  8.4411,   8.4142/)

  ! Set box in 1d array
  box(1:9)=(/15., 0., 0., 0., 15., 0., 0., 0., 15./)

  ! Set optimizer type
  opt_type='adam'
  ! Set number of images, and total natoms of images 
  nimages=1
  nAtimg=8

  ! Set energy/force training .true./.false.
  energy_training = .TRUE.
  force_training = .TRUE.

  ! Initiate training: read mlff.pyamff, allocate arrays for NN and backward propagations
  CALL train_init(nAtoms, nelement, atomicNumbers, uniqueNrs)
  ! Set target energy/force values after train_init
  targetE(1)=-21.920866
  targetF(1:3,1)=(/ 0.8799140000,  1.5469880000,  0.8036050000/)
  targetF(1:3,2)=(/-1.6183470000,  0.2690820000,  0.4712980000/)
  targetF(1:3,3)=(/-0.1497420000,  0.3694370000,  1.7673110000/)
  targetF(1:3,4)=(/0.7095770000, -1.3030910000,  0.9290810000/)
  targetF(1:3,5)=(/ 0.3905060000,  0.6688260000, -2.2920590000/)
  targetF(1:3,6)=(/-1.2745240000,  1.4202270000, -1.2543460000/)
  targetF(1:3,7)=(/-1.2145130000, -2.0276030000, -0.7726570000/)
  targetF(1:3,8)=(/2.2771290000, -0.9438650000,  0.3477670000/)

  ! Set force_coeff (optional)
  force_coeff=0.02

  ! Execution of training over images. Trainer is executed only when i=nimages
  ! From i=1, nimages-1, execute neighborlist, fingerprints, and forward propagation 
  DO i=1, nimages
    img_idx=i
    CALL trainExec(nAtoms,pos_car,box,atomicNumbers,nelement,uniqueNrs,opt_type,max_epoch,force_coeff)
  END DO

  ! Deallocate all allocated arrays
  CALL traincleanup


END PROGRAM
