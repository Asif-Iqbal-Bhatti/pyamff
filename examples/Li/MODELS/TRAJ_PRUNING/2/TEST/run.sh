#!/bin/sh
#
#SBATCH -p shared
#SBATCH -N 1
#SBATCH -n 32
#SBATCH -t 20:00:00
#SBATCH -J TEST_2
#SBATCH -o ll_out
#SBATCH -A che190010

python3 debug.py train_with_all.traj
