import numpy as np
from pyamff.aseCalc import aseCalc
import torch, sys
from ase.io import read

args = sys.argv
images = read(args[1], index=":")

calc = aseCalc('./pyamff.pt5400')

es = open('es.dat','w')
fs = open('fs.dat','w')
e_j = 0
f_j = 0
f_rmse = 0
e_rmse = 0
for atoms in images:
  print(len(atoms), e_j, flush=True)
  dft_e = atoms.get_potential_energy()
  dft_fs = atoms.get_forces()
  atoms.set_calculator(calc)
  ml_e = atoms.get_potential_energy()
  ml_fs = atoms.get_forces()
  e_rmse = e_rmse + ((dft_e - ml_e)/len(atoms))**2
  e_j = e_j + 1
  es.write("{:12.6f} {:12.6f} {:12.6f} {:12.6f} \n".format(dft_e, dft_e/len(atoms), ml_e, ml_e/len(atoms)))
  for dft_f, ml_f in zip(dft_fs, ml_fs):
     f_rmse_internal = 0
     for i in range(3):
       f_rmse_internal = f_rmse_internal + ((dft_f[i]-ml_f[i])**2)
       fs.write("{:12.6f} {:12.6f} \n".format(dft_f[i], ml_f[i]))
     f_rmse_internal = f_rmse_internal/(len(atoms))
     f_rmse = f_rmse + f_rmse_internal
print("number of images", len(images))
print("f_rmse : ", np.sqrt(f_rmse/(3*len(images))))
print("e_rmse per atom", np.sqrt(e_rmse))
print("e_rmse ", np.sqrt(e_rmse/len(images)))

