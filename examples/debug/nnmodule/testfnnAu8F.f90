PROGRAM pyamff
  USE fpCalc
  USE normalize
  USE fnnmodule 

  IMPLICIT NONE
  
  INTEGER, PARAMETER :: nAtoms=8
  INTEGER, PARAMETER :: MAX_FPs = 20
  INTEGER, PARAMETER :: MAX_NEIGHS = 48
  DOUBLE PRECISION,DIMENSION(nAtoms,3) :: pos_car
  INTEGER, DIMENSION(nAtoms) :: symbols
  DOUBLE PRECISION,DIMENSION(3,3) :: cell
  DOUBLE PRECISION,DIMENSION(3) :: cell1
  DOUBLE PRECISION,DIMENSION(3) :: cell2
  DOUBLE PRECISION,DIMENSION(3) :: cell3
  DOUBLE PRECISION,DIMENSION(3) :: atom1
  DOUBLE PRECISION,DIMENSION(3) :: atom2
  DOUBLE PRECISION,DIMENSION(3) :: atom3
  DOUBLE PRECISION,DIMENSION(3) :: atom4
  DOUBLE PRECISION,DIMENSION(3) :: atom5
  DOUBLE PRECISION,DIMENSION(3) :: atom6
  DOUBLE PRECISION,DIMENSION(3) :: atom7
  DOUBLE PRECISION,DIMENSION(3) :: atom8
  
  INTEGER, PARAMETER :: nelement=1 
  CHARACTER*20 :: filename
  CHARACTER*3, DIMENSION(nelement) :: uniq_elements
  INTEGER, DIMENSION(nAtoms) :: num_neigh
  INTEGER, DIMENSION(nAtoms, MAX_NEIGHS) :: neighs
  DOUBLE PRECISION, DIMENSION(nAtoms, MAX_FPS) :: fps
  DOUBLE PRECISION, DIMENSION(nAtoms, MAX_NEIGHS, 3, MAX_FPS) :: dfps !Can we change this into reverse order? 
  
  REAL :: st1, f1, st2, f2, st3, f3
  INTEGER :: j, k, l
  filename = 'fpParas.dat'
  uniq_elements(1) = 'Au'
  print *, 'fp defined'

  !TODO:Read in structures
  DATA cell1 /20.0000000000000000,    0.0000000000000000,    0.0000000000000000/
  DATA cell2 /0.0000000000000000,   20.0000000000000000,    0.0000000000000000/
  DATA cell3 /0.0000000000000000,    0.0000000000000000,   20.0000000000000000/
  cell(1,:) = cell1
  cell(2,:) = cell2
  cell(3,:) = cell3
  DATA atom1 /7.04265,  6.3964,   7.4517/
  DATA atom2 /9.58385,  7.1238,   6.7005/
  DATA atom3 /8.48345,  7.4005,   4.2486/
  DATA atom4 /7.18605,  8.8166,   6.1653/
  DATA atom5 /6.64055,  7.2958,  10.7514/
  DATA atom6 /9.26035,  6.0553,   9.2278/
  DATA atom7 /8.37835,  8.9447,   8.7524/
  DATA atom8 /5.41615,  8.4411,   8.4142/
  
  pos_car(1,:) = atom1
  pos_car(2,:) = atom2
  pos_car(3,:) = atom3
  pos_car(4,:) = atom4
  pos_car(5,:) = atom5
  pos_car(6,:) = atom6
  pos_car(7,:) = atom7
  pos_car(8,:) = atom8
  DATA symbols /1, 1, 1, 1, 1, 1, 1, 1/
  print *, 'image defined'
   
  CALL read_fpParas(filename, nelement)

  CALL calcfps(nAtoms, pos_car, cell, symbols, MAX_FPs,&
               fps, dfps, neighs, num_neigh)

  print *, 'fp calc done'             
  
  CALL normalizeFPs(nelement, nAtoms, uniq_elements, symbols, MAX_FPS, MAXVAL(num_neigh), fps, dfps(:,1:MAXVAL(num_neigh)+1,:,:))

  CALL cpu_time(st1)
  ! Neural network 
  CALL prepfNN(nAtoms, nelement, symbols, uniq_elements, MAX_FPS, MAXVAL(num_neigh))
  CALL cpu_time(f1)
  PRINT *, 'Time for prepfNN is ', f1-st1
  CALL cpu_time(st2)
  CALL forwardCalc(num_neigh, neighs(:,1:MAXVAL(num_neigh)), fps, dfps(:,1:MAXVAL(num_neigh)+1,:,:))
  CALL cpu_time(f2)
  PRINT *, 'Time for forward is ', f2-st2
  CALL cpu_time(st3)
  CALL nncleanup
  CALL cpu_time(f3)
  PRINT *, 'Time for cleanup is ', f3-st3

END PROGRAM
