PROGRAM testfppara

  USE io

  IMPLICIT NONE
  INTEGER :: i, j, k
  CHARACTER*20 :: filename
  INTEGER, PARAMETER :: nelement = 2
  CHARACTER*3, DIMENSION(nelement) :: uniq_elements

  TYPE (fingerprints), DIMENSION(nelement) :: fpParas

  filename = 'fpParas.dat'
  uniq_elements(1) = 'H'
  uniq_elements(2) = 'Pd'

  CALL read_fpParas(filename, nelement, uniq_elements, fpParas)
  print *, 'G1s'
  DO i=1, nelement
    DO j=1, nelement
      print *, fpParas(i)%g1s(j)%etas
      print *, fpParas(i)%g1s(j)%startpoint
    ENDDO
  ENDDO

  print *, 'G2s'
  DO i=1, nelement
    DO j=1, nelement
      DO k=1, nelement
        print *, fpParas(i)%g2s(j,k)%etas
        print *,j,k, fpParas(i)%g2s(j,k)%startpoint
        print *,j,k, fpParas(i)%g2s(j,k)%endpoint
      ENDDO
    ENDDO
  ENDDO
END PROGRAM
