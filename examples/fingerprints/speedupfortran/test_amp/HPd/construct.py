"""
This calculator is only used to set up forces and energy read from a file
"""
from __future__ import division

import numpy as np

from ase.calculators.calculator import Calculator, all_changes
from ase.calculators.calculator import PropertyNotImplementedError
from ase.io import read, Trajectory


class force_setter(Calculator):
    implemented_properties = ['energy', 'forces']
    default_parameters = {'epsilon': 1.0,
                          'sigma': 1.0,
                          'rc': None}
    nolabel = True

    def __init__(self, energy=None, forces=None,**kwargs):
        Calculator.__init__(self, **kwargs)
        self.energy = energy
        self.forces = forces

    def calculate(self, atoms=None, 
                  properties=['energy'],
                  system_changes=all_changes):
        Calculator.calculate(self, atoms, properties, system_changes)

#        natoms = len(self.atoms)
        
#        positions = self.atoms.positions
#        cell = self.atoms.cell

        self.results['energy'] = self.energy
        self.results['forces'] = self.forces

images = read('POSCAR_h1pd2', index=':', format='vasp')

traj = Trajectory('h1pd2.traj','w')
for i in range(1):
 for image in images:
  calc = force_setter(energy=0.0, forces=np.zeros((3,3)))
  image.set_calculator(calc)
  image.get_potential_energy()
  image.get_forces()
  traj.write(image)
