"""Simple test of the Amp calculator, using Gaussian descriptors and neural
network model. Randomly generates data with the EMT potential in MD
simulations."""

import os,sys, time, shutil
from ase import Atoms, Atom, units
from amp.utilities import (make_filename, hash_images, Logger, string2dict,
                        logo, now, assign_cores, TrainingConvergenceError,
                        check_images)
import ase.io
import numpy as np
from ase.optimize.lbfgs import LBFGS
from ase.optimize.fire import FIRE
#from ase.calculators.lammpslib import LAMMPSlib
from ase.calculators.emt import EMT
from ase.md.langevin import Langevin
from ase.lattice.surface import fcc110
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase.md import VelocityVerlet
from ase.constraints import FixAtoms
#from expectra.atoms_operator import match
from amp import Amp
from amp.descriptor.gaussian import Gaussian, make_symmetry_functions
from amp.descriptor.cutoffs import Cosine,Polynomial
#from amp.model.neuralnetwork import NeuralNetwork
#from amp.model.tflow import NeuralNetwork
from amp.model import LossFunction

#etas = 160  120 120   40    800   400   400   400   400
#Rs   =  0   0.5  1.0  2.0   3.2   3.5   4.0   5.0   6.0
#40.0_0.0    100.0_0.0   300.0_0.0   600.0_0.0   700.0_1.1   800.0_1.5
Gs = {
      "Pd": [
             {"type":"G2", "element":"H", "eta":100., "Rs":0.5},
             {"type":"G2", "element":"Pd", "eta":100., "Rs":0.5},
             {"type":"G4", "elements":["H", "Pd"], "eta":0.005, "gamma":1.0, "zeta":6.0, "theta_s":-0.}
            ],
      "H": [
             {"type":"G2", "element":"Pd", "eta":0.05, "Rs":0.0},
             {"type":"G4", "elements":["H", "Pd"], "eta":0.005, "gamma":1.0, "zeta":6.0, "theta_s":-0.}
           ]
     }



descriptor=Gaussian(Gs=Gs,
                               cutoff=Cosine(6.0))
                               #cutoff=Polynomial(gamma=5, Rc=3.0)
#calc = Amp.load('amp-untrained-parameters.amp', cores=12)
#os.system('rm amp-untrained-parameters.amp')
ttime = 0
for i in range(1):
   images = hash_images('pd1h2.traj', log=None)
   check_images(images, forces=True)
   st=time.time()
   descriptor.calculate_fingerprints(
           images=images,
           parallel={'cores':1},
           log=None,
           calculate_derivatives=True)
   ttime += time.time()-st
   shutil.rmtree('./amp-data-fingerprint-primes.ampdb')
   shutil.rmtree('./amp-data-neighborlists.ampdb')
   shutil.rmtree('./amp-data-fingerprints.ampdb')
print('TIME USED', ttime)
