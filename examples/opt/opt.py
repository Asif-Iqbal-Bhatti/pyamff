import tsase
import numpy as np
#from pyamff.aseCalc import aseCalc
from pyamff.activeLearn import activeLearn
import torch, sys
from ase.io import read
from pyamff.aseCalcF import aseCalcF
import ase
from math import sqrt
#args = sys.argv
#images = read(args[1], index=":")

#atoms = read('pos_0099.con')
atoms = read('pos_0000.con')

#train = activeLearn('./config.ini')

#calc1 = tsase.calculators.morse(rc=9.5)

#atoms.set_calculator(calc1)
#atoms.get_potential_energy()
#f = atoms.get_forces()
#print(f)
#images = []

#images.append(atoms)

#opt = ase.optimize.LBFGS(atoms)
#opt.run(fmax=0.01, steps=1)
#images.append(atoms)
#opt.run(fmax=0.01,steps=1)
#images.append(atoms)


#traj=[images[0:2], images[2:4], images[4:6]]
#traj=[images[0:2], images[0:4]]
#traj=images

#fmax = 10000000
#while fmax >= 0.1:
#    print('Images: ', images)
#    print('Training NN')
#    train.train(images)
#    print('Training finished')
calc2 = aseCalcF()

    
atoms.set_calculator(calc2)
es = atoms.get_potential_energy()
fs = atoms.get_forces()
print(fs)

#x0 = [None] * 256
#for i in range(len(x0)):
#      x0[i] = (np.random.uniform(-0.01,0.01), np.random.uniform(-0.01,0.01), np.random.uniform(-0.01,0.01))
#pos = atoms.get_positions()
#atoms.set_positions(pos+x0)
#e2 = atoms.get_potential_energy()
#print(e2)

#    fm = sqrt((fs ** 2).sum(axis=1).max())
#    print(fm)
#    print('Optimizing on NN')
opt = ase.optimize.LBFGS(atoms, maxstep=0.1)
opt.run(fmax=0.01, steps=10)
#    print('Optimization on NN Finished')
#    atoms.set_calculator(calc1)
#    e = atoms.get_potential_energy()
#    f = atoms.get_forces()
    #print(e)
#    fmax = sqrt((f ** 2).sum(axis=1).max())
#    print('fmax at end of loop:', fmax)  
#    images.append(atoms)
    
#    train.train(images)
""" 
weights = {}
bias = {}
params = []
l1_weight = [[-0.06245638, -0.02177071], [ 0.01106360, -0.02641876]]
l1_bias   = [-0.15927899, 0.08015668]
l2_weight = [[-0.01062437, -0.00506942],[0.00031429, -0.12331749]]
l2_bias   = [0.18475361, 0.17465138]
l3_weight = [[-0.10928684, 0.02496535]]
l3_bias   = [0.12033310]

l1_weight1 = [[-0.12125415, -0.09574964], [-0.12175528,-0.01690174]]
l1_bias1   = [-0.00525357, 0.16651967]
l2_weight1 = [[0.14360507, -0.03717202],[0.18517374,-0.01645058]]
l2_bias1   = [0.03095685, -0.14551263]
l3_weight1 = [[-0.12728526, 0.03061981]]
l3_bias1   = [-0.00860174]

params = [l1_weight, l1_bias, l2_weight, l2_bias, l3_weight, l3_bias,
          l1_weight1, l1_bias1, l2_weight1, l2_bias1, l3_weight1, l3_bias1]

#calc.train(images, debug=True, params=params)
debug=True
for image in traj:
   calc.train(image)  #, debug=debug, params=params)
   #debug = False
   #print('params', params)
"""
