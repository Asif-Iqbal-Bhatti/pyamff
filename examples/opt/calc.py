import tsase
import numpy as np
#from pyamff.aseCalc import aseCalc
from pyamff.activeLearn import activeLearn
import torch, sys
from ase.io import read
from pyamff.aseCalcF import aseCalcF
import ase
from math import sqrt
#args = sys.argv
#images = read(args[1], index=":")

#atoms = read('pos_0099.con')
atoms = read('pos_0000.con')

weights = {}
bias = {}
params = []

l1_weight = [[8.13780753e-02, -1.05594200e-01, -6.71366382e-02, 6.03854291e-02, -2.02120538e-01, 2.64164734e-01, 2.58600687e-01, 1.52018527e-01],[2.20538498e+00, 1.54357095e+00, -3.31906039e-01, -4.16170006e+00, -2.56449315e-02, -3.73924469e+00, 3.30272828e+00, -5.01619005e+00], [1.84007004e+00, 1.16183266e+00, -8.24529064e-01, -2.61213895e+00, -7.73364336e-01, -2.76739722e+00, 1.87789673e+00, -2.99324897e+00], [3.59518166e-01, 2.96603159e-01, 1.23291630e-01, 8.89731266e-01, 2.87816290e-01, 7.96243900e-01, 5.50526928e-01, 1.02067496e+00], [6.32066640e-01, 7.10710084e-01, 6.23897325e-01, 1.47050401e+00, 7.30637395e-01, 1.48025669e+00, 8.91946165e-01, 1.72911047e+00]]
l1_bias = [-9.98711399e+00, -8.70337080e+00, -6.62576834e+00, -8.53381412e+00, -1.05983987e+01] 
l2_weight = [[-1.36069810e-01, 4.58306359e-01, -1.22316194e+00, -1.47049236e-01, -3.93258384e-01], [-1.16117978e+00, -1.19423369e+00, -9.42957005e-01, -8.83916785e-01, -9.80422527e-01], [-4.18220363e+01, -4.40855961e+01, -4.02676771e+01, -4.04228009e+01, -3.92961598e+01], [2.20048631e+01, 2.31738876e+01, 2.12580128e+01, 2.14787521e+01, 2.04621212e+01], [-1.90933671e+01, -2.02673839e+01, -1.81492117e+01, -1.84374554e+01, -1.79957971e+01]] 
l2_bias = [4.16158720e+00, 1.95237628e+01, -8.02998227e+01, 4.21338744e+01, -1.47774781e+02]
l3_weight = [[-3.06717180e+03, -3.16095989e+03, -3.43748362e+03, -3.32249851e+03, -2.98617420e+03]]
l3_bias = [-6.39833667e+03]

params = [l1_weight, l1_bias, l2_weight, l2_bias, l3_weight, l3_bias]




train = activeLearn('./config.ini',optim=True,params=params)

calc1 = tsase.calculators.morse(rc=9.5)

atoms.set_calculator(calc1)
atoms.get_potential_energy()
f = atoms.get_forces()
#print(f)
images = []

images.append(atoms)
natoms = len(images[0])

f_out = open('results.dat','w')
f_out.write('Energy   fMax \n')

fmax = sqrt((f ** 2).sum(axis=1).max())
step = 0

while fmax >= 0.01:
    print('Images: ', images)
    print('Training NN')
    train.train(images)
    fRMSE = train.calc.forceRMSE
#    f_trust = fRMSE * natoms
    f_stop = fmax - fRMSE
#    print(f_trust)
#    print(f_stop)
#    sys.exit()
    print('Training finished')
    if step != 0:
        calc2.cleanup()

    calc2 = aseCalcF()

    
    atoms.set_calculator(calc2)
    es = atoms.get_potential_energy()
    fs = atoms.get_forces()
#print(fs)
#    fm = sqrt((fs ** 2).sum(axis=1).max())
#    print(fm)
    print('Optimizing on NN')
    opt = ase.optimize.LBFGS(atoms, maxstep=0.01)
    opt.run(fmax=f_stop, steps=100)
    print('Optimization on NN Finished')
    atoms.set_calculator(calc1)
    e = atoms.get_potential_energy()
    f = atoms.get_forces()
    #print(e)
    fmax = sqrt((f ** 2).sum(axis=1).max())
    f_out.write('{}     {}\n'.format(e, fmax))
    print('fmax at end of loop:', fmax)  
    images.append(atoms)
    step +=1
#    train.train(images)



""" 
weights = {}
bias = {}
params = []
l1_weight = [[-0.06245638, -0.02177071], [ 0.01106360, -0.02641876]]
l1_bias   = [-0.15927899, 0.08015668]
l2_weight = [[-0.01062437, -0.00506942],[0.00031429, -0.12331749]]
l2_bias   = [0.18475361, 0.17465138]
l3_weight = [[-0.10928684, 0.02496535]]
l3_bias   = [0.12033310]

l1_weight1 = [[-0.12125415, -0.09574964], [-0.12175528,-0.01690174]]
l1_bias1   = [-0.00525357, 0.16651967]
l2_weight1 = [[0.14360507, -0.03717202],[0.18517374,-0.01645058]]
l2_bias1   = [0.03095685, -0.14551263]
l3_weight1 = [[-0.12728526, 0.03061981]]
l3_bias1   = [-0.00860174]

params = [l1_weight, l1_bias, l2_weight, l2_bias, l3_weight, l3_bias,
          l1_weight1, l1_bias1, l2_weight1, l2_bias1, l3_weight1, l3_bias1]

#calc.train(images, debug=True, params=params)
debug=True
for image in traj:
   calc.train(image)  #, debug=debug, params=params)
   #debug = False
   #print('params', params)
"""
