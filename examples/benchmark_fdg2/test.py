#!/usr/bin/env python
import os
from pyamff.config import ConfigClass
from ase.io import Trajectory, read
from pyamff.fingerprints.fingerprints import Fingerprints
import numpy as np

#Read and set up setting parameters
config = ConfigClass()
config.initialize()
fp_paras = config.config['fp_paras'].fp_paras
nFPs = {}
for key in fp_paras.keys():
    nFPs[key] = len(fp_paras[key])
fpcalc = Fingerprints(uniq_elements=config.config['fp_paras'].uniq_elements, filename=config.config['fp_parameter_file'], nfps=nFPs)

image = read(config.config['trajectory_file'],index=0, format='vasp')
orig_image = image.copy()
chemsymbols = image.get_chemical_symbols()
fps, dfps = fpcalc.calcFPs(atoms=image, chemsymbols=chemsymbols)

dx = 0.0001 #perturbation

image[0].position = image[0].position + np.array([dx, 0.,0.])
fps1, dfps_1 = fpcalc.calcFPs(atoms=image, chemsymbols=chemsymbols)
print(fps)
print('numerical d')
print((fps1 - fps)/dx)
for key in dfps.keys():
   print(key, dfps[key])
