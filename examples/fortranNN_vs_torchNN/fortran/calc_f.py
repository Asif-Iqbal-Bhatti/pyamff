import numpy as np
from pyamff.aseCalcF import aseCalcF
import torch, sys
from ase.io import read

args = sys.argv
images = read(args[1], index=":")

calc  = aseCalcF('./pyamff.pt')

es = open('es_f.dat','w')
fs = open('fs_f.dat','w')
for atoms in images:
    #print ('positions')
    #print (atoms.get_positions())
    dft_e = atoms.get_potential_energy()
    dft_fs = atoms.get_forces()
    atoms.set_calculator(calc)
    ml_e = atoms.get_potential_energy()
    ml_fs = atoms.get_forces()
    es.write("{:12.6f} {:12.6f} \n".format(dft_e, ml_e))
    for dft_f, ml_f in zip(dft_fs, ml_fs):
        for i in range(3):
            fs.write("{:12.6f} {:12.6f} \n".format(dft_f[i], ml_f[i]))

