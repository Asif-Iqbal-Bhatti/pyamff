import numpy as np
from pyamff.aseCalc import aseCalc
import torch, sys
from ase.io import read

args = sys.argv
images = read(args[1], index=":")

calc  = aseCalc('./pyamff.pt')

es = open('es.dat','w')
fs = open('fs.dat','w')
for atoms in images:
  dft_e = atoms.get_potential_energy()
  dft_fs = atoms.get_forces()
  atoms.set_calculator(calc)
  ml_e = atoms.get_potential_energy()
  ml_fs = atoms.get_forces()
  es.write("{:12.6f} {:12.6f} \n".format(dft_e, ml_e))
  for dft_f, ml_f in zip(dft_fs, ml_fs):
     for i in range(3):
       fs.write("{:12.6f} {:12.6f} \n".format(dft_f[i], ml_f[i]))

