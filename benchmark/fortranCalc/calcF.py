from pyamff.aseCalcF import aseCalcF
import sys
from ase.io import read

args = sys.argv
images = read(args[1], index=":")

calc  = aseCalcF()

i=0
#saveFF(calc.model, calc.preprocessParas, filename="mlff.pyamff")
for img in images:
  img.set_calculator(calc)
  print('image:',i)
  #print(img.get_positions())
  print(img.get_potential_energy())
  print(img.get_forces())
  i+=1
