"""
This calculator is used to load a FORTRAN trained machine-learning model and do calculations
"""
from __future__ import division

import numpy as np

from ase.neighborlist import NeighborList
from ase.calculators.calculator import Calculator, all_changes
from pyamff.utilities.preprocessor import generateInputs, normalize
from ase.calculators.calculator import PropertyNotImplementedError
from pyamff.mlModels.pytorchNN import NeuralNetwork
from pyamff.utilities.preprocessor import normalizeParas
from pyamff.neighborlist import NeighborLists
from pyamff.config import ConfigClass
from pyamff.fingerprints.fingerprintsWrapper import atomCenteredFPs
import torch

from pyamff import fmodules

class aseCalcF(Calculator):
    implemented_properties = ['energy', 'forces']
    default_parameters = {}
    nolabel = True

    def __init__(self, modelType='NeuralNetwork', read_mlff=True, mlff_file='mlff.pyamff', **kwargs):
        Calculator.__init__(self, **kwargs)
        self.max_fps = 100
        self.mlff_file = mlff_file
        fmodules.pyamff.read_file = read_mlff

    def set_maxfps(self, max_fps):
        self.max_fps = max_fps

    def set_mlff(self):
        fmodules.pyamff.read_file = True

    def calculate(self, atoms=None,
                  properties=['energy'],
                  system_changes=all_changes):
        Calculator.calculate(self, atoms, properties, system_changes)
        energy, forces = self.calculateFingerprints(self.atoms)

        self.results['energy'] = energy
        self.results['forces'] = forces

    def calculateFingerprints(self, atoms=None):
        atomicNrs = np.array(atoms.get_atomic_numbers())
        uniqueNrs = []
        for i in atomicNrs:
            if i not in uniqueNrs:
                uniqueNrs.append(i)
        unique = np.array(uniqueNrs)
        pos_car = atoms.get_positions()
        cell = atoms.cell.array.flatten()
        predForces, predEnergies = fmodules.pyamff.calc_ase(pos_car, cell, atomicNrs, unique, self.mlff_file)
        fmodules.pyamff.read_file = False 
        return predEnergies, predForces

    def cleanup(self):
        #fmodules.neuralnetwork.nncleanup_optim()
        fmodules.fpcalc.cleanup_ase()
