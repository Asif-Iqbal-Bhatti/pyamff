#!/usr/bin/env python3
from pyamff.pyamffRunner import pyamffRunner

def main():
    pyamff = pyamffRunner()
    pyamff.run()
if __name__ == "__main__":
    main()
