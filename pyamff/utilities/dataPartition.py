import numpy as np
import torch
import copy, math
from pyamff.fingerprints.fingerprintsWrapper import atomCenteredFPs
from .fileIO import loadData
from collections import OrderedDict
import random, time
import os
import pickle

def partitionData(indexes, nBatches, seed=None):
    """
    Partition fp data to 'nBatches' batches
    """
    batches = {}
    sizes = [1.0 / nBatches for _ in range(nBatches)]
    data_len = len(indexes)
    if seed:
        random.seed(seed)
        random.shuffle(indexes)
    batchID = 0
    for frac in sizes:
        part_len = int(frac*data_len)
        batches[batchID] = indexes[0:part_len]
        indexes = indexes[part_len:]
        batchID+=1
    if len(indexes)>0:
        #print("Warning batch size of one partition is smaller than others: %d vs %d"%(len(indexes), part_len), flush=True) 
        batchID = 0
        for index in indexes:
            batches[batchID].append(index)
            batchID+=1
    """
    for frac in sizes:
        part_len = int(frac * data_len)
        if self.inmemory:
            batches[batchID]  =  self.preprocess(indexes[0:part_len], batchID)
        else:
            self.preprocess(indexes[0:part_len], batchID)
            batches[batchID] = batchID
        print('indexes:', batchID, indexes[0:part_len])
        indexes = indexes[part_len:]
        batchID+=1
        if frac == sizes[-1] and len(indexes) < part_len and len(indexes)>0:
            print("Warning batch size of one partition is smaller than others: %d vs %d"%(len(indexes), part_len), flush=True) 
    """
    return batches

class Dataset(torch.utils.data.Dataset):
    'Characterizes a dataset for PyTorch'

    internal_cache = {}
    def __init__(self, data, srcDir, indices):
          'Initialization'
          self.data = data
          self.srcDir = srcDir
          self.indices = indices
          self.internal_cache = {}

    def __len__(self):
          'Denotes the total number of samples'
          return len(self.indices)

    def __getitem__(self, index):
          'Generates one sample of data'
          # Select sample
          #print('fetch', self.indices[index])
          if index in self.internal_cache:
              return self.internal_cache[index]

          fname = os.path.join(self.srcDir,
                               'batches_{}.pckl'.format(self.indices[index]))
          with open(fname, 'rb') as f1:
              X = pickle.load(f1)
          #X = self.data[self.indices[index]]
          self.internal_cache[index] = X
          return X

class DataPartitioner(object):
    """
    Used to split data based on number of process
    srcData: a list of keys that used to store fp data
    """
    def __init__(self, srcData, fpRange, magnitudeScale,
                 interceptScale, nProc, fpDir=None, nBatches=None, device=None, seed=1234, test=False, st=None, useExisting=False):
        #print('%.2fs: Entering dataPartition'%(time.time()-st))
        d = device # i dont want to send device variable everywhere hence made it d
        if isinstance(srcData, str):
            self.inmemory = True
            data = load_data(filename=srcData, rb='rb')
            self.fpRange = data['fpRange']
            self.fpData = data['fpData']
        if isinstance(srcData, list):
            self.inmemory = False
            self.fpData = srcData 
            self.fpRange = fpRange
            self.magnitudeScale = magnitudeScale
            self.interceptScale = interceptScale
        self.partitions = []
        if fpDir is None:
            if test: # for train_testFF
                self.srcDir = os.getcwd() + '/test_fingerprints'
            else:
                self.srcDir = os.getcwd() + '/fingerprints'
        else:
            self.srcDir = fpDir
        if test: # for train_testFF
            self.batchDir = os.getcwd() + '/test_batches'
        else:
            self.batchDir = os.getcwd() +'/batches'
        if not os.path.exists(self.batchDir):
            os.mkdir(self.batchDir)
        #print ("BATCH NUMBER: ",batch_numb)
        if nBatches:
            if self.inmemory:
                indexes = list(self.fpData.keys())
            else:
                indexes = copy.copy(self.fpData)
            batches = partitionData(indexes, nBatches, seed)
            for key in batches.keys():
                #print('indexes:', key, batches[key])
                if self.inmemory:
                    batches[batchID] = self.preprocess(batches[key], d, key, useExisting)
                else:
                    self.preprocess(batches[key], d, key, useExisting)
                    batches[key] = key
            self.fpData = batches
        self.partitionBatches(nProc, seed)
        #print('%8.2fs: Data partition done' % (time.time()-st))


    def preprocess(self, dataIDs, device, batchID, useExisting=False):
        acfs = []
        # for any mode, if batches exist, then use it
        # also read only if useExisting is true
        if useExisting:
            fname = os.path.join(self.batchDir, 'batches_{}.pckl'.format(batchID))
            if os.path.exists(fname):
                with open(fname, 'rb') as f1:
                    batch = pickle.load(f1)
                    return batch

        for dataID in dataIDs:
            fname = os.path.join(self.srcDir, 'fps_{}.pckl'.format(dataID))
            with open(fname, 'rb') as f1:
                acf = pickle.load(f1)
                #acf.normalizeFPsList(self.fpRange, self.magnitudeScale, self.interceptScale)
                acf.normalizeFPs(self.fpRange, self.magnitudeScale, self.interceptScale)
                acfs.append(acf)
        batch = atomCenteredFPs()
        batch.stackFPs(acfs)
        if device:
            batch.toTensor(device=device) # we use this only when GPU is used for training, otherwise the if statement is skipped
        fname = os.path.join(self.batchDir, 'batches_{}.pckl'.format(batchID))
        with open(fname, 'wb') as f1:
            pickle.dump(batch, f1)
        return batch

    # Partition data based on number of process
    def partitionBatches(self, nProc, seed=1234):
       """
       Partition batches to 'nProc' set
       """
       sizes = [1.0 / nProc for _ in range(nProc)]
       random.seed(seed)
       indexes = list(self.fpData.keys())
       data_len = len(indexes)
       random.shuffle(indexes)
       for frac in sizes:
           part_len = int(frac * data_len)
           self.partitions.append(indexes[0:part_len])
           indexes = indexes[part_len:]

    def use(self, rank):
        return Dataset(self.fpData, self.batchDir, self.partitions[rank])

# collate_fn: imagesFPs: a list of atomsCenteredFps
def batchGenerator(acfs):
    st = time.time()
    if len(acfs) == 1: # i dont know why do we check this? if and regular statement is exactly the same.
        #print (acfs)
        #print (acfs[0])
        batch = atomCenteredFPs()
        batch.stackFPs(acfs)
        return batch
    batch = atomCenteredFPs()
    #print("Batch: ",batch)
    #print ('ACFS: ',acfs)
    batch.stackFPs(acfs)
    #print("After Batch: ",batch)
    #batch.stackFPsList(acfs)
    #print(' BatchingTIMEUSED:', time.time()-st)
    return batch

